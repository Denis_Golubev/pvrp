This repository contains a video animation of the solution process of the implementation belonging to the paper "Solving the Physical Vehicle Routing Problem
for Improved Multi-Robot Freespace Navigation" by Stefan Edelkamp, Denis Golubev, and Christoph Greulich.
